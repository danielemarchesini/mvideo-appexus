jQuery(function($, undefined) {
  // multilingual
  var src  = document.location.search.replace('/','').replace('?','').toLowerCase().split('&'),
      pars = [],
      tmp  = '',
      key  = 'lang',    /* language parameter name */
      lang = undefined, /* admitted value: it|en */
      tbox = $('.multilingual-text');

  for(var s in src){
    tmp = src[s].split('=');
    if(tmp[0] == key){ lang = tmp[1] }
    pars.push(tmp);
  }

  if(!!lang && !!$('.multilingual-text[data-lang='+lang+']').length){
    tbox.hide();
    $('.multilingual-text[data-lang='+lang+']').show(function(){
      var $this = $(this);
      if ($this.hasClass('app-store') || $this.hasClass('google-play')) {
        $this.css({ 'display' : 'block' });
      };
      if ($this.hasClass('button') || $this.hasClass('footer-link') || $this.hasClass('footer-text')) {
        $this.css({ 'display' : 'inline-block' });
      };
    });
  }

  // target _blank
  $('[data-target]').each(function(){
    var $this = $(this);
    $this.attr('target',$this.attr('data-target'));
  });

  // code replacement
  // $('.code-box-content[data-replace-text=true]').each(function(){
  //   var $this = $(this);
  //   var code = $this.html();
  //   code = code.replace(/</g, '&lt;').replace(/>/g, '&gt;');
  //   $this.html(code);
  // });

  // code injection
  $('.code-box-content[data-replace-text=true]').each(function(){
    var $this     = $(this),
        _url      = $this.data('code-location'),
        _content  = undefined,
        _class    = undefined;

    $.ajax({ url: _url, dataType: 'text'})
          .done(function(data) {
            _class    = 'correct';
            _content  = data;
          })
          .error(function(error) {
            _class    = 'error';
            _content  = error.statusText;
          })
          .complete(function(){
            console.log($this.prev().append(' for: <span class="file-name">'+_url+'</span>'));
            $this.html('<form><textarea class="code-display '+_class+'">'+_content+'</textarea></form>');
          });
  });

  // data visualization
  $('[data-accordion=true]').each(function(){
    var $this = $(this);
    $this.on('click',function(evt){
      evt.preventDefault();
      var _parent = $this.parent(),
          _parameters = _parent.find('.parameters-list');
      _parameters.toggle();
    });
  });
});
