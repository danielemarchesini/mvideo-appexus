// interstitial

var createContent = function(){
  var $div     = document.createElement('div'),
      divStyle = 'position: fixed; top: 0; left: 0; z-index: 1000; width: 100%; height: 100%; text-align: center; background-color: #{OVERLAY_BGCOLOR};';
  $div.id  = 'interstitial-wr';
  $div.setAttribute('style', divStyle);


//  var $cnt     = document.createElement('div'),
//      cntStyle = '';
//  $cnt.id  = 'cnt-wr';
//  $cnt.setAttribute('style', cntStyle);
//  $cnt.innerHTML = escape('${CONTENT}').toString();

  var $img     = document.createElement('img'),
      imgStyle = 'position: absolute; left: 50%; top: 50%; margin: calc(-#{IMAGE_HEIGHT}px/2) 0 0 calc(-#{IMAGE_WIDTH}px/2); border: #{IMAGE_BORDER_WIDTH}px #{IMAGE_BORDER_COLOR} solid;';
  $img.setAttribute('style', imgStyle);
  $img.setAttribute('alt', 'alternate text');
  $img.setAttribute('title', 'image title');
  $img.src = '#{IMAGE_URL}';
  document.getElementsByTagName('body')[0].appendChild($div);
  document.getElementById('interstitial-wr').appendChild($img);
//  document.getElementById('interstitial-wr').appendChild($cnt);
}

window.onload = createContent;
